#pragma once
#include <stdint.h>
#include <stdbool.h>

#define ISR(name) void isr_ ## name (void) __critical __interrupt(__COUNTER__)

void isr_nop() __naked;

void init_im2();
uint8_t register_isr(uint8_t idx, void* handler); // idx=0xff to auto-assign a free one, ret=idx or 0xff on error

uint8_t pad_isr_idx(uint8_t idx, uint8_t mask);
