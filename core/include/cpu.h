#pragma once
#include <stdint.h>

inline void EI() { __asm__("ei"); }
inline void DI() { __asm__("di"); }


// 0= use IN/OUT instructions (currently impossible)
//    sdcc fix your shit https://sourceforge.net/p/sdcc/bugs/3160/
// 1= helper functions:
//    tiny bit slower than 2
// 2= inline assembler & global vars:
//    a lot bigger than 1
#define CPU_H_INOUT_MODE 1

#if CPU_H_INOUT_MODE == 0
  #define OUT(addr,val) (*((__sfr uint8_t *) addr))=((uint8_t)val)
  #define IN(addr) (*((__sfr uint8_t *) addr))
#elif CPU_H_INOUT_MODE == 1
  void OUT(uint8_t addr, uint8_t val);
  uint8_t IN(uint8_t addr);
#elif CPU_H_INOUT_MODE == 2
  extern uint8_t cpu_h_io_addr, cpu_h_io_val;
  inline void OUT(uint8_t addr, uint8_t val){
    cpu_h_io_addr=addr;
    cpu_h_io_val=val;
    __asm
      push af
      push bc
      ld a, (_cpu_h_io_addr)
      ld c, a
      ld a, (_cpu_h_io_val)
      out (c), a
      pop bc
      pop af
    __endasm;
  }
  inline uint8_t IN(uint8_t addr){
    cpu_h_io_addr=addr;
    __asm
      push af
      push bc
      ld a, (_cpu_h_io_addr)
      ld c, a
      in a, (c)
      ld (_cpu_h_io_val), a
      pop bc
      pop af
    __endasm;
    return cpu_h_io_val;
  }
#else
  #error "CPU_H_INOUT_MODE"
#endif
