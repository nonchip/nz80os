#pragma once
#include <stdint.h>

extern void* int_vector_table[128];
extern char* crt0_magicstr;
extern uint8_t crt0_magicver[3];
extern void crt0_break();
extern void crt0_end_of_CODE();
extern void crt0_end_of_DATA;
