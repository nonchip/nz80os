void isr_nmi() __critical __interrupt;

void isr_r08() __critical;
void isr_r10() __critical;
void isr_r18() __critical;
void isr_r20() __critical;
void isr_r28() __critical;
void isr_r30() __critical;
void isr_r38() __critical;

extern void (*isr_nmi_dispatch)();
extern void (*isr_r08_dispatch)();
extern void (*isr_r10_dispatch)();
extern void (*isr_r18_dispatch)();
extern void (*isr_r20_dispatch)();
extern void (*isr_r28_dispatch)();
extern void (*isr_r30_dispatch)();
extern void (*isr_r38_dispatch)();
