#warning "replace hwconfig.h and stuff using it with actual driver structs"

#define HW_CTC_BASE $80
#define HW_CTC_CH0 HW_CTC_BASE
#define HW_CTC_CH1 HW_CTC_BASE+1
#define HW_CTC_CH2 HW_CTC_BASE+2
#define HW_CTC_CH3 HW_CTC_BASE+3

#define HW_SIO_BASE $84
#define HW_SIO_AD HW_SIO_BASE
#define HW_SIO_AC HW_SIO_BASE+1
#define HW_SIO_BD HW_SIO_BASE+2
#define HW_SIO_BC HW_SIO_BASE+3
