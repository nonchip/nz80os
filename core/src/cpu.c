#include "core/include/cpu.h"

#if CPU_H_INOUT_MODE == 1
  void OUT(uint8_t addr, uint8_t val) __naked {
    addr; val; // compiler shut up
    __asm
      ld iy, #2   ; set up iy for stack access past return address
      add iy, sp
      push bc
      ld b, #0    ; make empty
      ld c, 0(iy) ; uint8_t addr
      ld a, 1(iy) ; uint8_t val
      out (c), a
      pop bc
      ret
    __endasm;
  }
  uint8_t IN(uint8_t addr) __naked {
    addr; // compiler shut up
    __asm
      ld iy, #2   ; set up iy for stack access past return address
      add iy, sp
      push bc
      ld b, #0    ; make empty
      ld c, 0(iy) ; uint8_t addr
      in a, (c)
      ld h, #0
      ld l, a     ; return (uint8_t)
      pop bc
      ret
    __endasm;
  }
#elif CPU_H_INOUT_MODE == 2
  uint8_t cpu_h_io_addr, cpu_h_io_val;
#endif
