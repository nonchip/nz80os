#include "core/include/resets.h"

void isr_nmi() __critical __interrupt {
  if(isr_nmi_dispatch != 0) isr_nmi_dispatch();
}

void isr_r08() __critical {
  if(isr_r08_dispatch != 0) isr_r08_dispatch();
}

void isr_r10() __critical {
  if(isr_r10_dispatch != 0) isr_r10_dispatch();
}

void isr_r18() __critical {
  if(isr_r18_dispatch != 0) isr_r18_dispatch();
}

void isr_r20() __critical {
  if(isr_r20_dispatch != 0) isr_r20_dispatch();
}

void isr_r28() __critical {
  if(isr_r28_dispatch != 0) isr_r28_dispatch();
}

void isr_r30() __critical {
  if(isr_r30_dispatch != 0) isr_r30_dispatch();
}

void isr_r38() __critical {
  if(isr_r38_dispatch != 0) isr_r38_dispatch();
}


void (*isr_nmi_dispatch)() = 0x0000;
void (*isr_r08_dispatch)() = 0x0000;
void (*isr_r10_dispatch)() = 0x0000;
void (*isr_r18_dispatch)() = 0x0000;
void (*isr_r20_dispatch)() = 0x0000;
void (*isr_r28_dispatch)() = 0x0000;
void (*isr_r30_dispatch)() = 0x0000;
void (*isr_r38_dispatch)() = 0x0000;
