#include "core/include/im2.h"
#include "core/include/crt0.h"

uint8_t boot_im2_free_idx;

void isr_nop () __naked {
  __asm__("reti");
}

void init_im2() {
  for (uint8_t i=0; i < 128; i++) {
    register_isr(i, &isr_nop);
  }

  boot_im2_free_idx = 0;

  __asm
    im 2
    ld a, #(_int_vector_table>>8)
    ld i, a
  __endasm;
}

uint8_t pad_isr_idx(uint8_t idx, uint8_t mask){
  if(idx == 0xff)
    idx = boot_im2_free_idx;
  while((idx&mask)!=0)
    idx++;
  if(idx >= 0x80)
    return 0xff;
  return idx;
}


uint8_t register_isr(uint8_t idx, void* handler){
  if(idx == 0xff)
    idx = boot_im2_free_idx;
  if(idx >= 0x80)
    return 0xff;
  int_vector_table[idx] = handler;
  boot_im2_free_idx = idx+1;
  return idx;
}
