;--------------------------------------------------------------------------
;  boot/crt0.s - NZ80OS bootloader c runtime
;  Copyright (C) 2020, Kyra Zimmer
;
;----- Derived from: ------------------------------------------------------
;
;  crt0.s - Generic crt0.s for a Z80
;
;  Copyright (C) 2000, Michael Hope
;
;  This library is free software; you can redistribute it and/or modify it
;  under the terms of the GNU General Public License as published by the
;  Free Software Foundation; either version 2, or (at your option) any
;  later version.
;
;  This library is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;  GNU General Public License for more details.
;
;  You should have received a copy of the GNU General Public License
;  along with this library; see the file COPYING. If not, write to the
;  Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
;   MA 02110-1301, USA.
;
;  As a special exception, if you link this library with other files,
;  some of which are compiled with SDCC, to produce an executable,
;  this library does not by itself cause the resulting executable to
;  be covered by the GNU General Public License. This exception does
;  not however invalidate any other reasons why the executable file
;   might be covered by the GNU General Public License.
;--------------------------------------------------------------------------

  .module crt0
  .globl  _bootloader
  .globl  _isr_nmi
  .globl  _isr_r08
  .globl  _isr_r10
  .globl  _isr_r18
  .globl  _isr_r28
  .globl  _isr_r20
  .globl  _isr_r28
  .globl  _isr_r30
  .globl  _isr_r38
  .globl  l__INITIALIZER
  .globl  s__INITIALIZED
  .globl  s__INITIALIZER

  ;; Ordering of segments for the linker.
  .area _HOME
  .area _CODE
  .area _INITIALIZER
  .area   _GSINIT
  .area   _GSFINAL
  .area _ENDOFROM

  .area _DATA
_int_vector_table::
  .blkw 128
  .area _INITIALIZED
  .area _BSEG
  .area   _BSS
  .area   _HEAP
  .area _ENDOFRAM

  .area _HEADER (ABS)
  .org  0x00
  jp  init

  .org  0x08
  jp _isr_r08

  .org  0x10
  jp _isr_r10

  .org  0x18
  jp _isr_r18

  .org  0x20
  jp _isr_r20

  .org  0x28
  jp _isr_r28

  .org  0x30
  jp _isr_r30

  .org  0x38
  jp _isr_r38

  .org 0x40
_crt0_magicstr::
  .asciz "NZ80OS"

  .org 0x50
_crt0_magicver::
  .db 0,0,0

  ;; function that simply returns, to be used as a breakpoint by detecting addr 0055
  .org 0x55
_crt0_break::
  ret

  .org 0x66
  JP _isr_nmi

___sdcc_call_hl::
  jp  (hl)

init:
  ;; Set stack pointer directly above top of memory.
  di
  ld  sp,#0000

  ;; Initialise global variables
  call  gsinit
  call  _bootloader
  jp  _exit

  .area   _CODE

_exit::
1$:
  halt
  jr  1$

  .area   _GSINIT
gsinit::
  ld  bc, #l__INITIALIZER
  ld  a, b
  or  a, c
  jr  Z, gsinit_next
  ld  de, #s__INITIALIZED
  ld  hl, #s__INITIALIZER
  ldir
gsinit_next:

  .area   _GSFINAL
  ret

  .area _ENDOFROM
_crt0_end_of_CODE::

  .area _ENDOFRAM
_crt0_end_of_DATA::
