# NZ80OS

a simple modular operating system / monitor / bootloader for Z80 computers such as the NZ80MOD

**extremely WIP!**

## Dependencies

### Software

* `sdcc`
* `make`
* optional: `minipro` for flashing
* optional: `z88dk-ticks` for simulating

## Design

the 3 parts (`core`,`kernel`,`user`) are concatenated as binaries and execution simply falls through

### `core/`

* `crt0`: barebones C runtime (sets SP, calls `main>bootloader`)
* `im2`: interrupt system
* `resets`: forwards resets (`NMI` and `RST 08..38`) as applicable
* `main`: initial bootloading (sets up `im2` and executes `kernel/main`)
* `cpu`: architecture specific helper functions (mostly IO because of a [sdcc bug](https://sourceforge.net/p/sdcc/bugs/3160/))

### `kernel/`

* `crt1`: generated "glue" C runtime for kernel that uses core addresses (requires relinking)
* `stream`: stream interface
  * kinda like linux "character devices" / "sockets"
  * can either read or write
  * no seeking/etc
* `pool`: pool interface
  * kinda like linux "block devices" / "files"
  * can create streams starting at certain addresses
* `zilog/`: low level drivers for the Zilog `Z84x0` peripheral line
  * `zidma`: `Z8410` DMA
  * `zipio`: `Z8420` PIO
  * `zictc`: `Z8430` CTC
  * `zisio`: `Z8440` SIO
* `syscall`: syscall infrastructure for `crt2` (using `RST38`)
* `main`: kernel entry point
  * loads modules
  * initializes `crt2` based userspace
  * executes `user/main`

### `user/`space

* `crt2`: "userspace" C runtime that uses syscalls (does not require relinking)
* `main`: userspace entry point / main loop (executes `forth`)
* `forth`: the Forth shell
