#pragma once
#include <stdint.h>
#include <stdbool.h>

struct stream;

struct stream_impl {
  bool (*get)(struct stream*, uint8_t*);
  bool (*put)(struct stream*, uint8_t*);
  bool (*close)(struct stream*);
};

struct stream {
  // flags
  uint8_t open     : 1;
  uint8_t readable : 1;
  uint8_t writable : 1;

  struct stream_impl* impl;
  void* userdata;
};

bool stream_put(struct stream*, uint8_t*);
bool stream_get(struct stream*, uint8_t*);
bool stream_close(struct stream*);

void stream_puts(struct stream*, char*);
