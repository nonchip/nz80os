#pragma once
#include <stdint.h>

#include "kernel/include/stream.h"

extern struct stream_impl zilog_sio_stream_impl;
extern struct stream      zilog_sioa_stream;
extern struct stream      zilog_siob_stream;

void zilog_siob_init_for_nz80mod();

//void zilog_siob_write_blocking(uint8_t data);

//uint8_t zilog_siob_read_blocking();
