#pragma once
#include <stdint.h>

union zilog_ctc_command {
  uint8_t word;
  struct {
    union { // sdcc fills bitfields lsb-first
      struct { // control
        uint8_t command   : 1; // 1=control, 0=vector
        uint8_t reset     : 1;
        uint8_t constant  : 1; // follows
        uint8_t trigger   : 1; // 1=on clk/trg, 0=auto on const
        uint8_t edgeselect: 1; // 1=rising, 0=falling
        uint8_t prescaler : 1; // 1=256, 0=16
        uint8_t counter   : 1; // 0=timer
        uint8_t interrupt : 1;
      };
      struct { // vector
        uint8_t _command  : 1; // 1=control, 0=vector
        uint8_t channel   : 2; // overridden by IC
        uint8_t vector    : 5; // 5bit of actual address
      };
    };
  };
};

void zilog_ctc_command(uint8_t address, union zilog_ctc_command* command);

void zilog_ctc_init_for_nz80mod();
