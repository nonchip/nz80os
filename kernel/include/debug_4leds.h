#pragma once
#include <stdint.h>
#include <stdbool.h>

void debug_4leds_setall(uint8_t all);
void debug_4leds_setone(uint8_t which, bool on);

void debug_4lets_die();
