  .module crt1
  .include "kernel/include/crt1.inc.gen"
  .globl _main
  .globl  l__INITIALIZER
  .globl  s__INITIALIZED
  .globl  s__INITIALIZER

;; Ordering of segments for the linker.

  .area _CODE
  .area _INITIALIZER
  .area   _GSINIT
  .area   _GSFINAL
  .area _ENDOFROM

  .area _DATA
  .area _INITIALIZED
  .area _BSEG
  .area   _BSS
  .area   _HEAP
  .area _ENDOFRAM

;; init code

  .area   _CODE
  call  gsinit
  call _main
  ret

  .area   _GSINIT
gsinit::
  ld  bc, #l__INITIALIZER
  ld  a, b
  or  a, c
  jr  Z, gsinit_next
  ld  de, #s__INITIALIZED
  ld  hl, #s__INITIALIZER
  ldir
gsinit_next:

  .area   _GSFINAL
  ret

  .area _ENDOFROM
_crt1_end_of_CODE::

  .area _ENDOFRAM
_crt1_end_of_DATA::
