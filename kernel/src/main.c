#include "kernel/include/crt1.h"
#include "kernel/include/stream.h"
#include "kernel/include/zilog/zictc.h"
#include "kernel/include/zilog/zisio.h"
#include "kernel/include/debug_4leds.h"

#include "core/include/crt0.h"
#include "core/include/resets.h"
#include "core/include/cpu.h"

void main() {
  // ugly hack for exposing the breakpoint function, RST38 will be full on syscall in the future!
  isr_r38_dispatch = &crt0_break;

  debug_4leds_setall(0); // turn off debug leds

  zilog_ctc_init_for_nz80mod();
  zilog_siob_init_for_nz80mod();

  EI();

  char* greeting = "Hello, World!\n";
  //while(1) stream_puts(&zilog_siob_stream, greeting);
  stream_put(&zilog_siob_stream, greeting);
  crt1_end_of_CODE();
}
