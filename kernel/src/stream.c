#include "kernel/include/stream.h"

bool stream_put(struct stream* stream, uint8_t* data){
  if(!stream->open)
    return false;
  if(!stream->writable)
    return false;
  return stream->impl->put(stream, data);
}

bool stream_get(struct stream* stream, uint8_t* data){
  if(!stream->open)
    return false;
  if(!stream->readable)
    return false;
  return stream->impl->get(stream, data);
}

bool stream_close(struct stream* stream){
  if(!stream->open)
    return false;
  return stream->impl->close(stream);
}

void stream_puts(struct stream* stream, char* data){
  if(!stream->open)
    return;
  if(!stream->writable)
    return;
  for (int i = 0; data[i]!=0; ++i)
  {
    while(!stream_put(stream, &data[i]));
  }
}
