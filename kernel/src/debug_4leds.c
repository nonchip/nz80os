#include "kernel/include/debug_4leds.h"
#include "core/include/cpu.h"

uint8_t debug_4leds_state;

inline void debug_4leds_send(){
  OUT(0xff,debug_4leds_state & 0x0f);
}

void debug_4leds_setall(uint8_t all){
  debug_4leds_state = all;
  debug_4leds_send();
}

void debug_4leds_setone(uint8_t which, bool on){
  debug_4leds_state = (debug_4leds_state & ~(1UL << which)) | ((on&1) << which);
  debug_4leds_send();
}

void debug_4leds_die(){
  debug_4leds_setall(1);
  __asm__("halt");
}
