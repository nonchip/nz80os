#include "kernel/include/zilog/zisio.h"
#include "kernel/include/debug_4leds.h"
#include "core/include/cpu.h"
#include "core/include/im2.h"

struct stream_impl zilog_sio_stream_impl;
struct stream      zilog_sioa_stream;
struct stream      zilog_siob_stream;

const static uint8_t sioAD = 0x84;
const static uint8_t sioAC = 0x85;
const static uint8_t sioBD = 0x86;
const static uint8_t sioBC = 0x87;

struct zilog_sio_ringbuffer {
  uint8_t buffer[16];
  uint8_t write: 4;
  uint8_t read: 4;
};

inline bool zilog_sio_ringbuffer_canread(struct zilog_sio_ringbuffer* buf){
  return buf->read != buf->write;
}

inline bool zilog_sio_ringbuffer_canwrite(struct zilog_sio_ringbuffer* buf){
  return (buf->read + 1) != buf->write;
}

inline uint8_t zilog_sio_ringbuffer_read(struct zilog_sio_ringbuffer* buf){
  return buf->buffer[buf->read++];
}

inline void zilog_sio_ringbuffer_write(struct zilog_sio_ringbuffer* buf, uint8_t data){
  buf->buffer[buf->write++]=data;
}

struct zilog_sio_channel_userdata {
  struct zilog_sio_ringbuffer *tx;
  struct zilog_sio_ringbuffer *rx;
  uint8_t ctrl_addr;
  uint8_t data_addr;
};

//struct zilog_sio_ringbuffer zilog_sioa_buf_tx;
//struct zilog_sio_ringbuffer zilog_sioa_buf_rx;
struct zilog_sio_ringbuffer zilog_siob_buf_tx;
struct zilog_sio_ringbuffer zilog_siob_buf_rx;
//struct zilog_sio_channel_userdata zilog_sioa_ud;
struct zilog_sio_channel_userdata zilog_siob_ud;

inline void zilog_sio_wr5_dtr(uint8_t control,bool dtr){
  OUT(control, 5);                     // WR5
  OUT(control, (dtr<<7) | 0b01101000); // dtr, 8bit, break off, tx on, crc 0, rts off, no crc
}

bool zilog_sio_stream_get(struct stream* stream, uint8_t* data){
  struct zilog_sio_channel_userdata* ud = stream->userdata;
  DI();
  if(!zilog_sio_ringbuffer_canread(ud->rx)){
    EI();
    return false;
  }
  *data = zilog_sio_ringbuffer_read(ud->rx);
  zilog_sio_wr5_dtr(ud->ctrl_addr, true);
  EI();
  return true;
}

bool zilog_sio_stream_put(struct stream* stream, uint8_t* data){
  struct zilog_sio_channel_userdata* ud = stream->userdata;
  DI();
  if(!zilog_sio_ringbuffer_canwrite(ud->tx)){
    EI();
    return false;
  }
  debug_4leds_setone(2,0);
  OUT(ud->ctrl_addr, 0);
  uint8_t status = IN(ud->ctrl_addr);
  if((!zilog_sio_ringbuffer_canread(ud->tx)) && (0!=(status&4))){
    OUT(ud->data_addr, *data);
    debug_4leds_setone(3,0);
    debug_4leds_setone(1,1);
  }else{
    zilog_sio_ringbuffer_write(ud->tx,*data);
    debug_4leds_setone(3,1);
    debug_4leds_setone(1,0);
  }
  EI();
  return true;
}

bool zilog_sio_stream_close(struct stream* stream){ stream; return false; /* we can't actually close a serial port */}
/*
ISR(zilog_sioa_txe){
  debug_4leds_setone(2,1);
  if(zilog_sio_ringbuffer_canread(&zilog_sioa_buf_tx))
    OUT(sioAD,zilog_sio_ringbuffer_read(&zilog_sioa_buf_tx));
}
ISR(zilog_sioa_sta){
  debug_4leds_setone(2,1);
  OUT(sioAC,0); // RR0
  IN(sioAC);
}
ISR(zilog_sioa_rxa){
  debug_4leds_setone(2,1);
  zilog_sio_ringbuffer_write(&zilog_sioa_buf_rx, IN(sioAD));
  if(!zilog_sio_ringbuffer_canwrite(&zilog_sioa_buf_rx))
    zilog_sio_wr5_dtr(sioAC, false);
}
ISR(zilog_sioa_src){
  debug_4leds_setone(2,1);
  OUT(sioAC,1); // RR1
  IN(sioAC);
  OUT(sioAC, 0b00110000); // crc nop, error reset, WR0
}
*/
ISR(zilog_siob_txe){
  debug_4leds_setone(2,1);
  //debug_4leds_setone(1,0);
  //debug_4leds_setone(3,0);
  OUT(sioBD,'x');
  //if(zilog_sio_ringbuffer_canread(&zilog_siob_buf_tx))
  //  OUT(sioBD,zilog_sio_ringbuffer_read(&zilog_siob_buf_tx));
}
ISR(zilog_siob_sta){
  //debug_4leds_setone(2,1);
  OUT(sioBC,0); // RR0
  IN(sioBC);
}
ISR(zilog_siob_rxa){
  //debug_4leds_setone(2,1);
  zilog_sio_ringbuffer_write(&zilog_siob_buf_rx, IN(sioBD));
  if(!zilog_sio_ringbuffer_canwrite(&zilog_siob_buf_rx))
    zilog_sio_wr5_dtr(sioBC, false);
}
ISR(zilog_siob_src){
  //debug_4leds_setone(2,1);
  OUT(sioBC,1); // RR1
  IN(sioBC);
  OUT(sioBC, 0b00110000); // crc nop, error reset, WR0
}

void zilog_siob_init_for_nz80mod(){
//  DI();
  uint8_t vector = pad_isr_idx(0xff,0b111);
  if(vector==0xff) // couldn't find a free vector
    debug_4leds_die();
  register_isr(vector+0b000, &isr_zilog_siob_txe); // B transmit buffer empty
  register_isr(vector+0b001, &isr_zilog_siob_sta); // B external status change
  register_isr(vector+0b010, &isr_zilog_siob_rxa); // B receive character available
  register_isr(vector+0b011, &isr_zilog_siob_src); // B special receive condition
  //register_isr(vector+0b100, &isr_zilog_sioa_txe); // A ...
  //register_isr(vector+0b101, &isr_zilog_sioa_sta);
  //register_isr(vector+0b110, &isr_zilog_sioa_rxa);
  //register_isr(vector+0b111, &isr_zilog_sioa_src);
  register_isr(vector+0b111, &isr_nop);

  OUT(sioBC, 0b00110000);   // error reset, WR0
  OUT(sioBC, 0b00011000|2); // channel reset, WR2
  OUT(sioBC, vector);
  OUT(sioBC, 0b00010000|4); // ext status reset, WR4
  OUT(sioBC, 0b01000100);   // /16 clock prescale (375000/16=23437.5baud), no sync mode, 1 stop bit, no parity
  OUT(sioBC, 3);            // WR3
  OUT(sioBC, 0b11000001);   // 8bit, no auto, no hunt, no rx crc, no address search, sync char load inhibit off, rx on
  zilog_sio_wr5_dtr(sioBC, true);
  OUT(sioBC, 0b00010000|1); // ext status reset, WR1
  OUT(sioBC, 0b00011111);   // interrupts: any rx(parity ignored), status affects vector, tx empty, external status

  //zilog_sioa_ud.tx=&zilog_sioa_buf_tx;
  //zilog_sioa_ud.rx=&zilog_sioa_buf_rx;
  //zilog_sioa_ud.ctrl_addr=sioAC;
  //zilog_sioa_ud.data_addr=sioAD;

  zilog_siob_ud.tx=&zilog_siob_buf_tx;
  zilog_siob_ud.rx=&zilog_siob_buf_rx;
  zilog_siob_ud.ctrl_addr=sioBC;
  zilog_siob_ud.data_addr=sioBD;

  zilog_sio_stream_impl.get = &zilog_sio_stream_get;
  zilog_sio_stream_impl.put = &zilog_sio_stream_put;
  zilog_sio_stream_impl.close = &zilog_sio_stream_close;

  zilog_sioa_stream.open = false;
  zilog_sioa_stream.readable = false;
  zilog_sioa_stream.writable = false;
  //zilog_sioa_stream.impl = &zilog_sio_stream_impl;
  //zilog_sioa_stream.userdata = &zilog_sioa_ud;

  zilog_siob_stream.open = true;
  zilog_siob_stream.readable = true;
  zilog_siob_stream.writable = true;
  zilog_siob_stream.impl = &zilog_sio_stream_impl;
  zilog_siob_stream.userdata = &zilog_siob_ud;

//  EI();

  //OUT(sioBC, 0); // RR0
  //uint8_t status = IN(sioBC);
  //stream_put(&zilog_siob_stream, &status);
}
