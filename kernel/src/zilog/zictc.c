#include "kernel/include/zilog/zictc.h"
#include "kernel/include/debug_4leds.h"
#include "core/include/cpu.h"
#include "core/include/im2.h"

//ISR(zilog_ctc_ch0){} // unused
//ISR(zilog_ctc_ch1){} // unused
//ISR(zilog_ctc_ch2){} // unused
ISR(zilog_ctc_ch3){ // systick
  static int timer_count = 0;
  static bool timer_led = false;
  if(timer_count++ >= 1000){
    timer_count=0;
    timer_led = !timer_led;
    debug_4leds_setone(0,timer_led);
  }
}

const uint8_t zilog_ctc_ch0 = 0x80;

void zilog_ctc_command(uint8_t address, union zilog_ctc_command* command){
  OUT(zilog_ctc_ch0+address, command->word);
}

void zilog_ctc_init_for_nz80mod(){
  union zilog_ctc_command cmd;

//  DI();

  uint8_t ivec = pad_isr_idx(0xff,0b11);
  if(ivec==0xff) // couldn't find a free vector
    debug_4leds_die();
//  register_isr(ivec+0b00, isr_zilog_ctc_ch0);
//  register_isr(ivec+0b01, isr_zilog_ctc_ch1);
//  register_isr(ivec+0b10, isr_zilog_ctc_ch2);
  register_isr(ivec+0b11, isr_zilog_ctc_ch3);

  // set up vector
  cmd.word = ivec;
  cmd.command = 0; // vector
  cmd.channel = 0; // set by ctc
  zilog_ctc_command(0, &cmd);

  // ch0 unused
  cmd.word=0;                     // clear
  cmd.command=1;                  // control
  cmd.reset=1;                    // reset
  zilog_ctc_command(0, &cmd);     // ch0 holding now

  // ch1 timer->SIOB
  cmd.word=0;                     // clear
  cmd.command=1;                  // control
  cmd.reset=1;                    // reset
  cmd.constant=1;                 // const follows
  zilog_ctc_command(1, &cmd);     // ch1 waiting for const
  cmd.word = 1;                   // 6000000/16(prescale)/1(value)=375000
  zilog_ctc_command(1, &cmd);     // ch1 is running

  // ch2 timer->ch3
  cmd.word=0;                     // clear
  cmd.command=1;                  // control
  cmd.reset=1;                    // reset
  cmd.constant=1;                 // const follows
  zilog_ctc_command(2, &cmd);     // ch2 waiting for const
  cmd.word = 3;                   // const is 3
  zilog_ctc_command(2, &cmd);     // ch2 is running

  // ch3 ch2->count->interrupt
  cmd.word=0;                     // clear
  cmd.command=1;                  // control
  cmd.reset=1;                    // reset
  cmd.constant=1;                 // const follows
  cmd.counter=1;                  // counter mode (from ch2)
  cmd.interrupt=1;                // emit interrupts
  zilog_ctc_command(3, &cmd);     // ch3 waiting for const
  cmd.word=125;                   // const is 125, makes for 1khz with ch2
  zilog_ctc_command(3, &cmd);     // ch3 is running

//  EI();
}
