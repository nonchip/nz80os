kernel/kernel.lib: \
  kernel/src/crt1.rel \
  kernel/src/main.o \
  kernel/src/zilog/zictc.o \
  kernel/src/zilog/zisio.o \
  kernel/src/stream.o \
  kernel/src/debug_4leds.o

kernel/include/crt1.inc.gen: core/core.map
	grep '^     0000' $< | grep '  _' | sed -E 's/^[^0]*0000([0123456789ABCDEF]*)  ([^ ]*).*$$/.globl \2\n\2 = #0x\1/' > $@

kernel/src/crt1.rel: kernel/src/crt1.s kernel/include/crt1.inc.gen
	$(AS) $(ASFLAGS) $@ $<

kernel/kernel.lnk.gen: core/core.map
	grep '^     0000' $< | grep '  _crt0_end_of_' | sed -E 's/^[^0]*0000([0123456789ABCDEF]*)  _crt0_end_of([^ ]*).*$$/-b \2 = 0x\1/' > $@
