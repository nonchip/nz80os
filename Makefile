TARGET = nz80os
AS = sdasz80
CC = sdcc
LD = sdldz80
AR = sdar
NM = sdnm
MKBIN = makebin
ASFLAGS = -l -o -s -I.
CCFLAGS = -mz80 --no-std-crt0 -c -I.
LDFLAGS = -n -m -w -i

.PHONY: all clean flash
all: $(TARGET).bin

include core/rules.mk
include kernel/rules.mk
include user/rules.mk

# --- generic
rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))
clean:
	rm $(call rwildcard,.,*.ihx *.gen *.bin *.map *.asm *.o *.rel *.lst *.sym *.lib *.sim)

%.lib:
	$(AR) -rc $@ $^

%.rel %.lst %.sym: %.s
	$(AS) $(ASFLAGS) $@ $<

%.o %.asm %.lst %.sym: %.c
	$(CC) $(CCFLAGS) -o $@ $<

%.ihx %.map: %.lnk %.lib
	$(LD) $(LDFLAGS) -f $< $@ $(filter-out $<,$^)
%.ihx %.map: %.lnk.gen %.lib
	$(LD) $(LDFLAGS) -f $< $@ $(filter-out $<,$^)

%.bin: %.ihx
	$(MKBIN) -p -s 32768 $< $@

$(TARGET).ihx: core/core.ihx kernel/kernel.ihx user/user.ihx
	head -q -n -1 $^ > $@
	echo ':00000001FF' >> $@

flash: $(TARGET).bin
	minipro -p AT28C256E -z -s -w $<

$(TARGET).sim: $(TARGET).bin
	z88dk-ticks -end 0055 -trace $< > $@
