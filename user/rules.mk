user/user.lib: \
	user/src/crt2.rel \
	user/src/main.o

user/src/crt2.rel: user/src/crt2.s
	$(AS) $(ASFLAGS) $@ $<

user/user.lnk.gen: kernel/kernel.map
	grep '^     0000' $< | grep '  _crt1_end_of_' | sed -E 's/^[^0]*0000([0123456789ABCDEF]*)  _crt1_end_of([^ ]*).*$$/-b \2 = 0x\1/' > $@
