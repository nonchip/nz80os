#include <stdint.h>

inline void breakpoint(void) {
  __asm__("RST 0x38"); // RST38 will be syscall in the future, right now it exposes the breakpoint function (addr 0055)
}

void main() {
  breakpoint();
  for(;;);
}
